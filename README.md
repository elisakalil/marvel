**👩🏻‍💻 Marvel Project**

**Projeto de um aplicação mobile desenvolvida em linguagem Swift com o intuito de capacitar desenvolvedoras iOS!**

Descubra quem são os personagens do mundo Marvel tendo acesso ao nome completo e descrição, são mais de 5 mil herois! Busque seus herois favoritos através do search bar e tenha acesso a eles mesmo sem conexão com a internet.

💻 Para visualizar o projeto no Figma [clique aqui](https://www.figma.com/file/P0mrpostVbzFnuOv9gra7K/Marvel?node-id=7%3A123). 

**Cronograma em fases de implementação:**

- 1️⃣ 4 dias **Setup Inicial: MVC + Navegação + Interfaces**
- 2️⃣ 4 dias **Integração com API**
- 3️⃣ 3 dias **Testes Unitários**
- 4️⃣ 5 dias **Persistência de Dados**
- 5️⃣ 3 dias **Filtro de Pesquisa**
- 6️⃣ 5 dias **Refactoring: Arquitetura MVC -> Arquitetura MVVM**
- 7️⃣ 2 dias **Teste unitário: síncrono**
- 8️⃣ 2 dias **Acessibilidade**
- 9️⃣ 2 dias **Tagueamento**

**Arquitetura, Tecnologias e Bibliotecas utilizadas**

- Xcode Version 13.0
- Versão mínima iOS 12.1
- Lottie (3.2.3)
- Kingfisher (7.1.1)
- MVC and MVVM Architecture

**Melhorias**

- Animação Spinner via biblioteca Lottie
- Ajuste de Networking: O usuário deve receber os dados da API quando tiver acesso a internet e do core data caso contrário.
- Acessibilidade: Home(colocar títulos, colocar número da listagem em ambas listas)
				  Na lista lateral(agrupar imagem com nome, fazer voice oveer falar que tem uma image e nome do herói)
				  Detalhes(agrupar as duplas de informações)

