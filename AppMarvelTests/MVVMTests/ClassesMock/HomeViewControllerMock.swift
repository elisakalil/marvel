
//  ViewControllerMock.swift
//  AppMarvelTests
//  Created by Elisa Kalil on 19/01/22.

import Foundation
@testable import AppMarvel

class HomeViewControllerMock: HomeViewControllerProtocol {
    
    var reloadCaroselCalledCount: Int = 0
    var reloadListCalledCount: Int = 0
   
    func reloadDataToCarousel() {
        reloadCaroselCalledCount += 1
    }
    
    func reloadDataToList() {
        reloadListCalledCount += 1
    }
}
