
//  CoreDataMock.swift
//  AppMarvelTests
//  Created by Elisa Kalil on 20/01/22.

import Foundation
import CoreData
@testable import AppMarvel
import Network

class CoreDataMock: CoreDataProtocol {
    
    var dataBase = DataBaseController()
    lazy var context = dataBase.persistentContainer.viewContext
    var offsetSaved: Bool = false
    var newRequestFromAPIInCoreDataSaved: Bool = false
    var getHerosFromCoreData: Bool = false
    var offset: Int32
    var result: [Result] = []
    
    init(offset: Int32) {
        self.offset = offset
    }
    
    func getOffset() -> Int32 {
        return offset
    }
    
    func saveOffset(offset: Int32) {
        self.offset = offset
        offsetSaved = true
    }
    
    func saveNewRequestFromAPIInCoreData(array: [Result]) {
        result.append(contentsOf: array)
        newRequestFromAPIInCoreDataSaved = true
    }
    
    func getNumberOfHerosPersistedInCoreData() -> Int {
        return result.count
    }
    
    func getHerosPersistedInCoreData() -> [HeroEntity] {

        let hero = HeroEntity(context: context)
        return Array(repeating: hero, count: result.count)
    }
    
    var dataHero: [HeroEntity] = []
    
}

