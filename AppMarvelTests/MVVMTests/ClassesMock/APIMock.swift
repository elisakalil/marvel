
//  APIMock.swift
//  AppMarvelTests
//  Created by Elisa Kalil on 19/01/22.

import Foundation
@testable import AppMarvel

class APIMock: APIProtocol {
    
    var json: String = "welcomeZero"
    
    func url(offset: Int32) -> String {
        return "ok"
    }
    
    func callAPI(urlString: String, completion: @escaping (Welcome) -> Void) {
        let bundle = Bundle(identifier: "com.kalilelisa.AppMarvelTests")
        guard let data = bundle?.url(forResource: json, withExtension: "json") else {return}
        guard let jsonData = try? Data(contentsOf: data) else {return}
        
        do {
            let response = try JSONDecoder().decode(Welcome.self, from: jsonData)
            completion(response)
        } catch {
            print (error.localizedDescription)
        }
    }
    
    var isRequest: Bool = false
}
