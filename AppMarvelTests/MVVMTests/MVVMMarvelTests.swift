
//  MVVMMarvelTests.swift
//  AppMarvelTests
//  Created by Elisa Kalil on 19/01/22.

import XCTest
@testable import AppMarvel

class MVVMMarvelTests: XCTestCase {
    
    let view = HomeViewControllerMock()
    let api = APIMock()

    func testRequestAPIFirstTime() {
        
        let coreData = CoreDataMock(offset: 0)
        let viewModel = HomeViewModel(view: view, api: api, coreData: coreData) 

        viewModel.requestAPI()

        XCTAssertEqual(viewModel.arrayOfHeros.count, 20)
        XCTAssertEqual(viewModel.arrayUp.count, 5)
        XCTAssertEqual(viewModel.arrayDown.count, 15)
        XCTAssertEqual(view.reloadCaroselCalledCount, 1)
        XCTAssertEqual(view.reloadListCalledCount, 1)
        XCTAssertEqual(coreData.offset, 20)

    }

    func testRequestAPISecondTime() {
        
        let coreData = CoreDataMock(offset: 0)
        let viewModel = HomeViewModel(view: view, api: api, coreData: coreData)

        viewModel.requestAPI()
        
        XCTAssertEqual(viewModel.arrayOfHeros.count, 20)
        
        api.json = "welcomeTwenty"
        viewModel.requestAPI()
        
        XCTAssertEqual(viewModel.helperHero.count, 40)
        XCTAssertEqual(viewModel.arrayDown.count, 35)
        XCTAssertEqual(view.reloadListCalledCount, 2)
        XCTAssertEqual(coreData.offset, 40)
    }
}

