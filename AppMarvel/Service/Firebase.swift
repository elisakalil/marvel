
//  FireBase.swift
//  AppMarvel
//  Created by Elisa Kalil on 01/02/22.

import Foundation
import Firebase

class Firebase: FirebaseProtocol {
    
    func screenView(screenName: String, screenClass: String) {
        Analytics.logEvent(AnalyticsEventScreenView,
                           parameters: [AnalyticsParameterScreenName: screenName,     
                                        AnalyticsParameterScreenClass: screenClass])
        
    }
    
    func selectedContent(itemName: String, contentType: String) {
        Analytics.logEvent(AnalyticsEventSelectContent,
                           parameters: [AnalyticsParameterItemName: itemName,
                                        AnalyticsParameterContentType: contentType,
        ])
    }
    
    func userID(userID: String) {
        let userID = Analytics.setUserID(userID)
    }
    
    func userProperty(fruta: String) {
        let fruta_favorita: String = ""
        Analytics.setUserProperty(fruta_favorita, forName: fruta)

    }
}
