
//  FirebaseProtocol.swift
//  AppMarvel
//  Created by Elisa Kalil on 01/02/22.

import Foundation

protocol FirebaseProtocol: AnyObject {
    func screenView(screenName: String, screenClass: String)
    func selectedContent(itemName: String, contentType: String)
    func userID(userID: String)
    func userProperty(fruta: String)
}
