
//  API.swift
//  AppMarvel
//  Created by Elisa Kalil on 26/11/21.

import Foundation
import UIKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG


class API: APIProtocol {
    
    var isRequest: Bool = false
    
    func MD5(string: String) -> String {
        
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func url(offset: Int32) -> String{
        
        let baseURL = "http://gateway.marvel.com"
        let path = "v1/public/characters"
        
        let publicKey = Bundle.main.object(forInfoDictionaryKey: "publicKey") as! String
        let privateKey = Bundle.main.object(forInfoDictionaryKey: "privateKey") as! String
        
        let ts = Int(Date().timeIntervalSince1970)
        
        let content = String(ts) + privateKey + publicKey
        let hash = MD5(string: content)
        
        let url = baseURL + "/" + path + "?" + "ts=\(ts)" + "&apikey=\(publicKey)" + "&hash=\(hash)" + "&offset=\(offset)"
        print(url)
        return url
    }
    
    func callAPI(urlString: String, completion: @escaping(Welcome) -> ()){
        
        if let url = URL(string: urlString) {
            isRequest = true
            URLSession.shared.dataTask(with: url) { data, response, error in
                self.isRequest = false
                guard let responseData = data else { return }
                
                do {
                    let post = try JSONDecoder().decode(Welcome.self, from: responseData)
                    completion(post)
                    
                } catch DecodingError.keyNotFound(let key, let context) {
                    print("could not find key \(key) in JSON: \(context.debugDescription)")
                    
                } catch DecodingError.valueNotFound(let type, let context) {
                    print("could not find type \(type) in JSON: \(context.debugDescription)")
                    
                } catch DecodingError.typeMismatch(let type, let context) {
                    print("type mismatch for type \(type) in JSON: \(context.debugDescription)")
                    
                } catch DecodingError.dataCorrupted(let context) {
                    print("data found to be corrupted in JSON: \(context.debugDescription)")
                    
                } catch let error as NSError {NSLog("Error in read(from:ofType:) domain= \(error.domain), description= \(error.localizedDescription)")
                }
            }.resume()
        }
    }
}
