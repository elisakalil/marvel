
//  APIProtocol.swift
//  AppMarvel
//  Created by Elisa Kalil on 19/01/22.

import Foundation

protocol APIProtocol: AnyObject{
    func url(offset: Int32) -> String
    func callAPI(urlString: String, completion: @escaping(Welcome) -> Void)
    var isRequest: Bool { get }
}
