
//  API.swift
//  AppMarvel
//  Created by Elisa Kalil on 26/11/21.

import Foundation

struct Welcome: Codable {
    let code: Int
    let status: String
    let data: DataClass
}

struct DataClass: Codable {
    let offset, limit, total, count: Int
    let results: [Result]
}

struct Result: Codable {
    let name: String
    let thumbnail: Thumbnail
    let stories: Stories
}

