
//  CoreDataController.swift
//  AppMarvel
//  Created by Elisa Kalil on 20/01/22.

import Foundation
import CoreData

class CoreDataController: CoreDataProtocol {
    
    var dataHero: [HeroEntity] = []
    let dataBase = DataBaseController()
    lazy var context: NSManagedObjectContext = {
            let context = dataBase.persistentContainer.viewContext
            return context
    }()
    lazy var homeViewModel: HomeViewModelProtocol  = {
        let homeViewModel = HomeViewModel(view: HomeViewController(), api: API(), coreData: self)
        return homeViewModel
    }()
    
    func getOffset() -> Int32 {
        var offset : Int32 = 0
        do {
            let result = try
            dataBase.persistentContainer.viewContext.fetch(OffsetEntity.fetchRequest())
            offset = result.last?.offset ?? 0
        } catch {
            print("Could not fetch data")
        }
        return offset
    }
    
    func saveOffset(offset: Int32) {
        let context = dataBase.persistentContainer.viewContext
        let newOffset = OffsetEntity(context: context)
        newOffset.offset = offset
        dataBase.saveContext()
    }
    
    func saveNewRequestFromAPIInCoreData(array: [Result]) {
        
        for hero in array {
            
            let context = dataBase.persistentContainer.viewContext
            let heroData = HeroEntity(context: context)
            heroData.name = hero.name
            
            let pathURL: String = hero.thumbnail.path
            let sizeURL: String = "standard_amazing"
            let extensionURL = hero.thumbnail.thumbnailExtension
            let completePath: String = "\(pathURL)/\(sizeURL).\(extensionURL)"
            heroData.image = completePath
            
            var description: String = ""
            for item in hero.stories.items {
                description += item.name + "\n"
            }
            
            dataBase.saveContext()
        }
    }
    
    func getHerosPersistedInCoreData() -> [HeroEntity] {
                
        do {
            self.dataHero = try dataBase.persistentContainer.viewContext.fetch(HeroEntity.fetchRequest())
            return dataHero
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return []
        }
    }
    
    func getNumberOfHerosPersistedInCoreData() -> Int {
        var numberOfHerosPersisted : Int = 0

        do {
            self.dataHero = try dataBase.persistentContainer.viewContext.fetch(HeroEntity.fetchRequest())
            numberOfHerosPersisted = dataHero.count

        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")

        }

        return numberOfHerosPersisted
    }
}
