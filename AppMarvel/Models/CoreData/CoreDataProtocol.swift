
//  CoreDataProtocol.swift
//  AppMarvel
//  Created by Elisa Kalil on 20/01/22.

import Foundation
import CoreData

protocol CoreDataProtocol: AnyObject {
    func getOffset() -> Int32
    func saveOffset(offset: Int32)
    func saveNewRequestFromAPIInCoreData(array: [Result])
    func getHerosPersistedInCoreData() -> [HeroEntity]
    func getNumberOfHerosPersistedInCoreData() -> Int
    
    var dataHero: [HeroEntity] { get set }
    var context: NSManagedObjectContext { get }
}
