
//  ListCollectionViewCell.swift
//  AppMarvel
//  Created by Elisa Kalil on 19/11/21.

import UIKit

class ListCell: UICollectionViewCell {
    
    @IBOutlet weak var listImage: UIImageView!
    @IBOutlet weak var listLabel: UILabel!
    
    var hasAddGradiente : Bool = false
    
    func setListCell(with herosList: HeroEntity){
        
        if let urlImage = herosList.image {
            let url = URL(string: urlImage)
            listImage.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [ .transition(.fade(1.0))], progressBlock: nil, completionHandler: nil)
        } else {
            listImage.image = UIImage(named: "placeholder")
        }
        
        listLabel.text = herosList.name
        
        listImage.layer.cornerRadius = 15
    }
    
    override func layoutSubviews() {
        
        if hasAddGradiente == false {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(0.9).cgColor,
                                    UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = listImage.frame
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.9)
            gradientLayer.endPoint = CGPoint(x: 0.1, y: 0.0)
            gradientLayer.cornerRadius = 10
            listImage.layer.insertSublayer(gradientLayer, at: 0)
            hasAddGradiente = true
        }
    }
}
