
//  CarouselCollectionViewCell.swift
//  AppMarvel
//  Created by Elisa Kalil on 19/11/21.

import UIKit
import Kingfisher

class CarouselCell: UICollectionViewCell {
    
    @IBOutlet weak var characterLabel: UILabel!
    @IBOutlet weak var characterImage: UIImageView!
    var hasAddGradiente : Bool = false
        
    func setCarouselCell(with herosList: HeroEntity){
        
        if let urlImage = herosList.image {
            let url = URL(string: urlImage)
            characterImage.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [ .transition(.fade(1.0))], progressBlock: nil, completionHandler: nil)
        } else {
            characterImage.image = UIImage(named: "placeholder")
        }
        
        characterLabel.text = herosList.name
        
        characterImage.layer.cornerRadius = 15
    }
    
    override func layoutSubviews() {
        
        if hasAddGradiente == false {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor,
                                    UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = characterImage.frame
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.9)
            gradientLayer.endPoint = CGPoint(x: 0.1, y: 0.0)
            gradientLayer.cornerRadius = 10
            characterImage.layer.insertSublayer(gradientLayer, at: 0)
            hasAddGradiente = true
        }
    }
}
