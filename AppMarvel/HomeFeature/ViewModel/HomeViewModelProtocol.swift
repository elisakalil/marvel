
//  HomeViewModelProtocol.swift
//  AppMarvel
//  Created by Elisa Kalil on 19/01/22.

import Foundation

protocol HomeViewModelProtocol {
    
    func requestAPI()
    func setDataToFillArray()
    func reloadMoreHeros()
    func lookingForCharacter(searchText: String?)
    
    var getArrayOfHeros: [Result] { get }
    var getArrayUp: [HeroEntity] { get }
    var getHelperHero: [HeroEntity] { get }
    var getArrayDown: [HeroEntity] { get }
    var getHeroFiltered: [HeroEntity] { get }
    
}


