
//  ViewModel.swift
//  AppMarvel
//  Created by ElisaKalil on 14/01/22.

import Foundation

class HomeViewModel: HomeViewModelProtocol {    
    
    var getArrayOfHeros: [Result] { return arrayOfHeros }
    var getArrayUp: [HeroEntity] { return arrayUp }
    var getArrayDown: [HeroEntity] { return arrayDown }
    var getHelperHero: [HeroEntity] { return helperHero }
    var getHeroFiltered: [HeroEntity] { return heroFiltered }
    
    var arrayOfHeros: [Result] = []
    var arrayUp: [HeroEntity] = []
    var arrayDown: [HeroEntity] = []
    var helperHero: [HeroEntity] = []
    var heroFiltered: [HeroEntity] = []
    
    private var view: HomeViewControllerProtocol
    private var api: APIProtocol
    private var coreData: CoreDataProtocol
    let dataBase = DataBaseController()
    
    init(view: HomeViewControllerProtocol, api: APIProtocol, coreData: CoreDataProtocol) {
        self.view = view
        self.api = api
        self.coreData = coreData
    }

    func requestAPI() {
        let urlString = api.url(offset: coreData.getOffset())
        api.callAPI(urlString: urlString) {(welcome) in
            
            self.arrayOfHeros = welcome.data.results
            self.coreData.saveNewRequestFromAPIInCoreData(array: self.arrayOfHeros)
            
            if self.coreData.getOffset() == 0 {
                self.coreData.dataHero = self.coreData.getHerosPersistedInCoreData()
                
                self.arrayUp.append(contentsOf: self.coreData.dataHero.prefix(5))
                self.arrayDown.append(contentsOf: self.coreData.dataHero.suffix(from: 5))
                
                self.view.reloadDataToCarousel()
                self.view.reloadDataToList()

            } else {
                self.helperHero = self.coreData.getHerosPersistedInCoreData()
                self.arrayDown.append(contentsOf: self.helperHero.suffix(from: Int(self.coreData.getOffset())))
                self.view.reloadDataToList()
            }
            
            self.coreData.saveOffset(offset: Int32(welcome.data.offset + welcome.data.count)) 
        }
    }
    
    func setDataToFillArray() {
        
        let numberOfHerosPersisted = coreData.getNumberOfHerosPersistedInCoreData()
        if numberOfHerosPersisted == 0 {
            self.requestAPI()
        } else {
            let dataHero = coreData.getHerosPersistedInCoreData()
            self.arrayUp.append(contentsOf: dataHero.prefix(5))
            self.arrayDown.append(contentsOf: dataHero.suffix(from: 5))
            self.view.reloadDataToCarousel()
            self.view.reloadDataToList()
        }
    }
    
    func reloadMoreHeros() {
        if api.isRequest == false {
            requestAPI()
            print("Resquesting API!")
        }
    }
    
    func lookingForCharacter(searchText: String?) {
        
        guard let text = searchText else { return }
            heroFiltered = arrayDown.filter { $0.name!.hasPrefix(text) }
            arrayDown = heroFiltered
            view.reloadDataToList()
        }
    }
    
    
