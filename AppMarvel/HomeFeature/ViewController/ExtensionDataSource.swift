
//  ExtensionDataSource.swift
//  AppMarvel
//  Created by Elisa Kalil on 14/01/22.

import Foundation
import UIKit

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == carouselCollection {
            return 5
        } else {
            return homeViewModel.getArrayDown.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == carouselCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carouselCell", for: indexPath) as! CarouselCell
            if homeViewModel.getArrayUp.count > 0 {
                let hero = homeViewModel.getArrayUp[indexPath.row]
                cell.setCarouselCell(with: hero)
            }
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as! ListCell
            if homeViewModel.getArrayDown.count > 0 {
                let hero = homeViewModel.getArrayDown[indexPath.row]
                cell.setListCell(with: hero)
            }
            
            return cell
        }
    }
}
