
//  HomeViewControllerDelegate.swift
//  AppMarvel
//  Created by Elisa Kalil on 14/01/22.

import Foundation
import UIKit
import Firebase

extension HomeViewController: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let detail = DetailsViewController()
        if collectionView == carouselCollection {
            detail.touchCharacter = homeViewModel.getArrayUp[indexPath.item]
            self.navigationController?.pushViewController(detail, animated: true)
            
        } else {
            detail.touchCharacter = homeViewModel.getArrayDown[indexPath.item]
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
}
