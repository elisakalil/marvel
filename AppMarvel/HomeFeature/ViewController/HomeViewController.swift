
//  ViewController.swift
//  AppMarvel
//  Created by Elisa Kalil on 19/11/21.

import UIKit
import CoreData
import Network
import Firebase

class HomeViewController: UIViewController, HomeViewControllerProtocol {
    
    let firebase: FirebaseProtocol
    lazy var homeViewModel: HomeViewModelProtocol  = {
        let homeViewModel = HomeViewModel(view: self, api: API(), coreData: CoreDataController())
        return homeViewModel
    }()
    
    init() {
        self.firebase = Firebase()
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var carouselCollection: UICollectionView!
    @IBOutlet weak var listCollection: UICollectionView!
    
    @IBOutlet weak var marvelCharactersListLabe: UILabel!
    @IBOutlet weak var featuredCharactersLabel: UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollections()
        configureNavigationBar()
        homeViewModel.setDataToFillArray()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        firebase.screenView(screenName: "Home", screenClass: "HomeViewController")
        
    }
    
    private func configureCollections() {
        carouselCollection.dataSource = self
        carouselCollection.delegate = self
        listCollection.dataSource = self
        listCollection.delegate = self
        self.searchBar.delegate = self
    }
    
    private func configureNavigationBar() {
        
        let logo = UIImageView(image: UIImage(named: "Marvel_Logo"))
        self.navigationItem.titleView = logo
        self.navigationController?.navigationBar.backgroundColor = .black
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        logo.widthAnchor.constraint(equalToConstant: 70).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    
    func reloadDataToCarousel() {
        DispatchQueue.main.async {
            self.carouselCollection.reloadData()
        }

    }

    func reloadDataToList() {
        DispatchQueue.main.async {
            self.listCollection.reloadData()
        }
    }
    
    func alertCathError(nameAlert: String) {
        DispatchQueue.main.async {
            let dialogMessage = UIAlertController(title: "Attention", message: nameAlert , preferredStyle: .alert)
            self.present(dialogMessage, animated: true, completion: nil)
        }
    }
}

