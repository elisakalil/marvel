
//  UISearchBarDelegate.swift
//  AppMarvel
//  Created by Elisa Kalil on 14/01/22.


import Foundation
import UIKit

extension HomeViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            homeViewModel.setDataToFillArray()
        } else {
            homeViewModel.lookingForCharacter(searchText: searchText)
        }}

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        homeViewModel.setDataToFillArray()
    }
}
    
