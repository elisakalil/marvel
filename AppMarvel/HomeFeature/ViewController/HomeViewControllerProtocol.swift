
//  HomeViewControllerDelegate.swift
//  AppMarvel
//  Created by Elisa Kalil on 14/01/22.

import Foundation

protocol HomeViewControllerProtocol: AnyObject {
    func reloadDataToCarousel()
    func reloadDataToList()
}
