
//  HomeViewControllerDelegate.swift
//  AppMarvel
//  Created by Elisa Kalil on 14/01/22.

import Foundation
import UIKit

extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let position = listCollection.contentOffset.y
        let listSize = listCollection.contentSize.height
        
        if position > listSize - listCollection.frame.size.height {
            homeViewModel.reloadMoreHeros()
        }
    }
}

