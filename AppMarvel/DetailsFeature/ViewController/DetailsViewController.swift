
//  Details.swift
//  AppMarvel
//  Created by Elisa Kalil on 22/11/21.


import Foundation
import UIKit
import Kingfisher
import CoreData
import Firebase

class DetailsViewController: UIViewController {
    
    let firebase: FirebaseProtocol
    
    init() {
        self.firebase = Firebase()
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var scrollView: UIScrollView = {
        let scrollview = UIScrollView()
        scrollview.translatesAutoresizingMaskIntoConstraints = false
        scrollview.backgroundColor = .black
        return scrollview
        
    }()
    
    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
        
    }()
    
    let imageDetails: DetailsGradienteViewController = {
        let image = DetailsGradienteViewController()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        return image
        
    }()
    
    let viewTitle: UIView = {
        let viewTitle = UIView()
        viewTitle.translatesAutoresizingMaskIntoConstraints = false
        return viewTitle
        
    }()
    
    let viewSubTitle: UIView = {
        let viewSubTitle = UIView()
        viewSubTitle.translatesAutoresizingMaskIntoConstraints = false
        return viewSubTitle
        
    }()
    
    let viewDescription: UIView = {
        let viewDescription = UIView()
        viewDescription.translatesAutoresizingMaskIntoConstraints = false
        return viewDescription
        
    }()
    
    let labelTitle: UILabel = {
        let labelTitle = UILabel()
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        labelTitle.textColor = .white
        labelTitle.font = UIFont.boldSystemFont(ofSize: 28.0)
        labelTitle.text = ""
        return labelTitle
        
    }()
    
    let labelSubTitle: UILabel = {
        
        let labelSubTitle = UILabel()
        labelSubTitle.translatesAutoresizingMaskIntoConstraints = false
        labelSubTitle.textColor = .white
        labelSubTitle.font = UIFont.boldSystemFont(ofSize: 14.0)
        labelSubTitle.text = labelSubTitle.text?.uppercased()
        return labelSubTitle
        
    }()
    
    let labelDescription: UILabel = {
        let labelDescription = UILabel()
        labelDescription.translatesAutoresizingMaskIntoConstraints = false
        labelDescription.textColor = .white
        labelDescription.font = labelDescription.font.withSize(14)
        labelDescription.text = ""
        labelDescription.numberOfLines = 0
        labelDescription.lineBreakMode = .byWordWrapping
        return labelDescription
        
    }()
    
    var touchCharacter : HeroEntity? {
        
        didSet {
            if let hero = touchCharacter {
                
                if let urlImage = hero.image {
                    let url = URL(string: urlImage)
                    imageDetails.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [ .transition(.fade(1.0))], progressBlock: nil, completionHandler: nil)
                } else {
                    imageDetails.image = UIImage(named: "placeholder")
                }
                
                labelTitle.text = hero.name
                labelDescription.text = hero.name
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        setUp()
        configureNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let nameHero: String = touchCharacter?.name ?? "Marvel Hero"
        
        firebase.screenView(screenName: "Details", screenClass: "DetailsViewController")
        firebase.selectedContent(itemName: nameHero.forSorting, contentType: "nome_heroi")
    }
    
    private func configureNavigationBar(){
        let logo = UIImageView(image: UIImage(named: "Marvel_Logo"))
        self.navigationItem.titleView = logo
        self.navigationController?.navigationBar.backgroundColor = .black
        
        logo.widthAnchor.constraint(equalToConstant: 70).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    private func setUp(){
        
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        scrollView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        stackView.addArrangedSubview(imageDetails)
        imageDetails.heightAnchor.constraint(equalToConstant: 385).isActive = true
        imageDetails.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        imageDetails.rightAnchor.constraint(equalTo: stackView.rightAnchor).isActive = true
        imageDetails.leftAnchor.constraint(equalTo: stackView.leftAnchor).isActive = true
        
        stackView.addArrangedSubview(viewTitle)
        viewTitle.addSubview(labelTitle)
        labelTitle.topAnchor.constraint(equalTo: viewTitle.topAnchor, constant: 24).isActive = true
        labelTitle.rightAnchor.constraint(equalTo: viewTitle.rightAnchor,constant: -24).isActive = true
        labelTitle.leftAnchor.constraint(equalTo: viewTitle.leftAnchor, constant: 24).isActive = true
        labelTitle.bottomAnchor.constraint(equalTo: viewTitle.bottomAnchor, constant: -24).isActive = true
        
        stackView.addArrangedSubview(viewSubTitle)
        viewSubTitle.addSubview(labelSubTitle)
        labelSubTitle.topAnchor.constraint(equalTo: viewSubTitle.topAnchor, constant: 24).isActive = true
        labelSubTitle.rightAnchor.constraint(equalTo: viewSubTitle.rightAnchor,constant: -24).isActive = true
        labelSubTitle.leftAnchor.constraint(equalTo: viewSubTitle.leftAnchor, constant: 24).isActive = true
        labelSubTitle.bottomAnchor.constraint(equalTo: viewSubTitle.bottomAnchor, constant: -24).isActive = true
        
        stackView.addArrangedSubview(viewDescription)
        viewDescription.addSubview(labelDescription)
        labelDescription.topAnchor.constraint(equalTo: viewDescription.topAnchor, constant: 0).isActive = true
        labelDescription.rightAnchor.constraint(equalTo: viewDescription.rightAnchor,constant: -24).isActive = true
        labelDescription.leftAnchor.constraint(equalTo: viewDescription.leftAnchor, constant: 24).isActive = true
        labelDescription.bottomAnchor.constraint(equalTo: viewDescription.bottomAnchor).isActive = true
    }
}

extension String {
    var forSorting: String {
        let simple = folding(options: [.diacriticInsensitive, .widthInsensitive, .caseInsensitive], locale: nil)
        let nonAlphaNumeric = CharacterSet.alphanumerics.inverted
        return simple.components(separatedBy: nonAlphaNumeric).joined(separator: "_")
    }
}
