
//  DetailsGradientViewController.swift
//  AppMarvel
//  Created by Elisa Kalil on 25/11/21.

import Foundation
import UIKit

class DetailsGradienteViewController: UIImageView {
    
    override func layoutSubviews() {
        let gradiente = CAGradientLayer()
        gradiente.frame = self.bounds
        gradiente.colors = [UIColor.clear.withAlphaComponent(0.0).cgColor,           UIColor.black.withAlphaComponent(0.9).cgColor]
        self.layer.addSublayer(gradiente)
    }
}
